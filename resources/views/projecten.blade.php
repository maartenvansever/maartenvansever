@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/index.css') }}" >
@endsection
@section('main')
    <div class="container mb-5 mx-auto w-full h-full">
        <div class="relative wrap overflow-hidden p-10 h-full">
            <div class="border-2-2 absolute border-opacity-20 border-gray-700 h-full border" style="left: 50%"></div>
            <!-- right timeline -->
            <div class="mb-8 flex justify-between items-center w-full right-timeline">
                <div class="order-1 w-5/12"></div>
                <div class="z-20 flex items-center order-1 bg-gray-800 shadow-xl w-8 h-8 rounded-full">
                    <h1 class="mx-auto font-semibold text-lg text-white">1</h1>
                </div>
                <div class="order-1 bg-gray-400 rounded-lg shadow-xl w-5/12 px-6 py-4">
                    <h3 class="mb-3 font-bold text-gray-800 text-xl">Stratego</h3>
                    <img src="{{ asset('images/projecten/stratego.png') }}" class="mb-3" alt="breakout" title="breakout">
                    <p class="text-sm leading-snug tracking-wide text-gray-900 text-opacity-100">
                        In het 2de semester heb ik  mijn eerste groepsproject gekregen, genaamd stratego.  Dit project werd gemaakt met  Java, Websockets, HTML, CSS en Javascript. Dit project werd gerealiseerd met 2 medestudenten, nl. Anton Baeckelandt en Florian Lambrecht.  Dit was ons eerste voorproefje over  hoe we Gitlab en Scrum konden gebruiken.
                    </p>
                </div>
            </div>

            <!-- left timeline -->
            <div class="mb-8 flex justify-between flex-row-reverse items-center w-full left-timeline">
                <div class="order-1 w-5/12"></div>
                <div class="z-20 flex items-center order-1 bg-gray-800 shadow-xl w-8 h-8 rounded-full">
                    <h1 class="mx-auto text-white font-semibold text-lg">2</h1>
                </div>
                <div class="order-1 bg-red-400 rounded-lg shadow-xl w-5/12 px-6 py-4">
                    <h3 class="mb-3 font-bold text-white text-xl">Toolrental</h3>
                    <img src="{{ asset('images/projecten/toolrental.png') }}" class="mb-3" alt="breakout" title="breakout">
                    <p class="text-sm font-medium leading-snug tracking-wide text-white text-opacity-100">
                        Dit is een project dat ik realiseerde voor het  vak ‘Webtechnlogie’. Ik bouwde een huursysteem waar mensen materialen van elkaar konden huren en uitlenen. Het uitlenen en huren van materialen werd vereffend in coins.  Dit project werd gemaakt in Laravel, HTML en CSS.</p>
                </div>
            </div>

            <!-- right timeline -->
            <div class="mb-8 flex justify-between items-center w-full right-timeline">
                <div class="order-1 w-5/12"></div>
                <div class="z-20 flex items-center order-1 bg-gray-800 shadow-xl w-8 h-8 rounded-full">
                    <h1 class="mx-auto font-semibold text-lg text-white">3</h1>
                </div>
                <div class="order-1 bg-gray-400 rounded-lg shadow-xl w-5/12 px-6 py-4">
                    <h3 class="mb-3 font-bold text-gray-800 text-xl">Matflix</h3>
                    <img src="{{ asset('images/projecten/phone.png') }}" class="mb-3" alt="breakout" title="breakout">
                    <p class="text-sm leading-snug tracking-wide text-gray-900 text-opacity-100">Als 2de project voor het vak ‘Webtechnologie’ heb ik  een Android app gemaakt die films weergeeft en deze films  in een favorietlijstje opneemt.  Dit project heb ik gemaakt in Android Studio met de taal Java en als backend Node.js.</p>
                </div>
            </div>

            <!-- left timeline -->
            <div class="mb-8 flex justify-between flex-row-reverse items-center w-full left-timeline">
                <div class="order-1 w-5/12"></div>
                <div class="z-20 flex items-center order-1 bg-gray-800 shadow-xl w-8 h-8 rounded-full">
                    <h1 class="mx-auto text-white font-semibold text-lg">4</h1>
                </div>
                <div class="order-1 bg-red-400 rounded-lg shadow-xl w-5/12 px-6 py-4">
                    <h3 class="mb-3 font-bold text-white text-xl">Breakout </h3>
                    <img src="{{ asset('images/projecten/breakout.png') }}"  class="mb-3" alt="breakout" title="breakout">
                    <p class="text-sm font-medium leading-snug tracking-wide text-white text-opacity-100">In het 4de semester van mijn 2de jaar heb ik met een team het ‘ Breakout’ spel gemaakt en gecommercialiseerd.  Ik heb geleerd hoeveel omzet je minstens moet behalen om dit project break-even te maken.  Het spel zat leuk in elkaar. De mensen konden micro transacties uitvoeren om skins te kopen.  Er kon gespeeld worden tegen andere mensen en wie de meeste vragen juist had, kreeg de meeste punten.
                    </p>
                </div>
            </div>
            <!-- right timeline -->
            <div class="mb-8 flex justify-between items-center w-full right-timeline">
                <div class="order-1 w-5/12"></div>
                <div class="z-20 flex items-center order-1 bg-gray-800 shadow-xl w-8 h-8 rounded-full">
                    <h1 class="mx-auto font-semibold text-lg text-white">5</h1>
                </div>
                <div class="order-1 bg-gray-400 rounded-lg shadow-xl w-5/12 px-6 py-4">
                    <h3 class="mb-3 font-bold text-gray-800 text-xl">Mars</h3>
                    <img src="{{ asset('images/projecten/trendingtopics.png') }}" class="mb-3" alt="breakout" title="breakout">
                    <p class="text-sm leading-snug tracking-wide text-gray-900 text-opacity-100">
                        Dit is een project dat ik in team gebouwd heb voor het vak ‘Trending Topics’.  Mensen kunnen onderdelen voor hun ‘marshuis’  kopen.  Dit project werd in meerdere talen gemaakt zoals Node.js, Mollie, React, Flutter, …
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
