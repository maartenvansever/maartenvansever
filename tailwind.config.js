const colors = require('tailwindcss/colors')
module.exports = {
    purge: [
        './storage/framework/views/*.php',
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        backgroundImage: {
            'banner': "url('/images/banner.jpg')",
        },
        screens: {
            'phone': '550px',
        }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
