@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/index.css') }}" >
    <link rel="stylesheet" href="{{ asset('css/show.css') }}" >
    <script src="/js/index.js"></script>
@endsection

@section('main')
    <h1 class="font-mono font-bold text-5xl mt-4 text-center text-yellow-900 mt-9">{{$blogs['title']}}</h1>
    <img src="{{$blogs['image']}}" class="w-5/12 ml-auto mr-auto mb-5 mt-9" alt="blogimage" title="blogimages">
    <div id="blogcontent" class="mb-12">
    {!!  $blogs['content'] !!}
    </div>
@endsection
