<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/default.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <title>Maarten Vansever</title>
</head>
<body class="bg-gray-50">
<main class="">
    <h1>U hebt een nieuwe mail ontvangen</h1>
    <p>afzender: {{ $email }}</p>
    <p>met als naam: {{ $name }}</p>
    <p>met als vraag: {{ $comment  }}</p>
</main>
</body>
</html>
