<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <title>Maarten Vansever</title>
</head>
<body class="bg-gray-50">
<header class="bg-yellow-900 pb-5">
<nav class="bg-yellow-900 text-center lg:h-24 sm:h-64">
    <ul class="leading-9 pt-7 flex flex-row justify-center sm:flex-col lg:flex-row">
        <li class="list-none ml-8 sm:mb-3 lg:mb-0">
            <a href="{{ route('home') }}" class="text-white font-bold text-2xl">Home</a>
        </li>
        <li class="ml-8 sm:mb-3">
            <a href="{{ route('cv') }}" class="text-white font-bold text-2xl">CV</a>
        </li>
        <li class="ml-8 sm:mb-3">
            <a href="{{ route('aboutme') }}" class="text-white font-bold text-2xl">Over Mij</a>
        </li>
        <li class="ml-8 sm:mb-3">
            <a href="{{ route('contact') }}" class="text-white font-bold text-2xl">Contact</a>
        </li>
        <li class="ml-8 sm:mb-3 pb-2">
            <a href="{{ route('projecten') }}" class="text-white font-bold  text-2xl">projecten</a>
        </li>
    </ul>
</nav>
    <div id="headercontent" class="flex justify-center mb-5 mt-5">
        <img src="{{asset('images/maarten.jpg')}}" class="rounded-full h-28 w-28 flex items-center justify-center" alt="Maarten Vansever" title="Maarten Vansever">
        <p class="mt-10 ml-4 text-white font-bold">"Programming isn't about what you know; it's about what you can figure out.” - Chris Pine</p>
    </div>
</header>
<main class="">
    <h2 class="font-mono font-bold text-4xl mt-5 ml-20 text-yellow-900">Blog</h2>
    <div class="blogs__wraper  py-16 px-10">
        <div class="flex justify-evenly items-center flex-wrap items-stretch">
        @foreach ($blogs as $blog)
            <div class="blogs bg-white mr-5 shadow-2xl w-96 mb-5 relative md:w-96">
                <div style="min-height:40vh;">
                    <img src="{{$blog['image']}}" class="mx-auto min-h-80"  alt="blogimage" title="blogimages">
                </div>
                <div class="p-5">
                    <h1 class="text-2xl font-bold text-yellow-900 py-2">{{{$blog['title']}}}</h1>
                    <p class="bg-white text-sm text-black mb-5 marginbottom">{!! $blog['shortdescription'] !!}</p>
                        <a href="{{ route('findblog', $blog['id']) }}" class="py-2 px-3 mt-4 mb-3 ml-5 px-6 text-white bg-yellow-900 inline-block rounded absolute bottom-0 left-0">Lees meer</a>
                        <div class="border-brown float-right mt-7 absolute right-0 bottom-0 mb-3 mr-5">
                            <span class="bg-yellow-900 text-white px-1 py-1 rounded-l-lg">clicks: </span>
                            <span class="px-1 py-1 rounded-r-lg text-left">{{{ $blog['clicks'] }}}</span>
                        </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

</main>
<footer class="bg-yellow-900 flex justify-center flex-col">
    <div class="flex justify-center flex-row">
        <a href="https://gitlab.com/maartenvansever" target="_blank">
            <i class="fab fa-gitlab text-white text-2xl w-16 hover:opacity-50"></i>
        </a>
        <a href="https://www.linkedin.com/in/maarten-vansever" target="_blank">
             <i class="fab fa-linkedin text-white text-2xl w-16 hover:opacity-50"></i>
        </a>
        <a href="mailto:maartenvansever@telenet.be" target="_blank" class="">
            <i class="far fa-envelope text-white text-2xl w-16 hover:opacity-50"></i>
        </a>
    </div>
    <div class="flex justify-center">
        <span class="text-white font-bold text-center">Made by Maarten Vansever</span>
    </div>
</footer>

</body>
</html>
