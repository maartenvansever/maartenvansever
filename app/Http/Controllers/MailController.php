<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{

    public function contactPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'message' => 'required',
            'name' => 'required'
        ]);
        if ($validator->fails()) {
           $key = "error";
           $boolean = true;
        } else {
           $key = "success";
           $boolean = true;
            Mail::send('email', [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'comment' => $request->get('message')
            ], function ($message) {
                $message->from('info@webdream.be');
                $message->to("maartenvansever@telenet.be", 'Maarten Vansever')
                    ->subject('Nieuwe mail van maartenvansever.be');
            });
        }
        return back()->with($key, $boolean);
    }
}
