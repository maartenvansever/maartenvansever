<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @return View
     */
    public function show ()
    {
       $result = [];
       foreach (Blog::all() as $key => $blog) {
               $result[$key]['shortdescription'] = Markdown::convertToHtml($blog->shortDescription);
               $result[$key]['title'] = $blog->title;
               $result[$key]['id'] = $blog->id;
               $result[$key]['image'] = $blog->image;
               $result[$key]['clicks'] = $blog->clicks;
       }
       $stop = 1;
        return view('index', ['blogs' => $result]);
    }

    /**
     * @param int $id
     * @return View
     */
    public function find (int $id)
    {
        $result = [];
        $blog = Blog::find($id);
        $blog->clicks += 1;
        $blog->save();
        $result['content'] = Markdown::convertToHtml($blog->content);
        $result['title'] = $blog->title;
        $result['id'] = $blog->id;
        $result['image'] = $blog->image;
        return view('show', ['blogs' => $result]);
    }

    public function aboutMe()
    {
        return view('about');
    }
    public function showCv()
    {
        return view('cv');
    }

    public function showContact()
    {
        return view('contact');
    }

    public function projecten()
    {
        return view('projecten');
    }
}
