<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'shortDescription',
        'content',
        'clicks',
        'image'
    ];

    public function getParsedDescriptionAttribute($value)
    {
        $parser = new \Parsedown();
        return $parser->text($this->description);
    }

}
