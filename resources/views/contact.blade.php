@extends('master')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
@endsection
@section('main')
    <script>

            @if(Session('success') === true)
                toastr.options =
                {
                    "closeButton" : true,
                    "progressBar" : true
                }
            toastr.success("Bedankt dat je contact met me hebt opgenomen, ik neem zo snel mogelijk contact met je op!");
            @elseif(Session('error') === true)
                toastr.options =
                {
                    "closeButton" : true,
                    "progressBar" : true
                }
            toastr.error("Gelieve alles in te vullen!");
            @else
                console.log('nothing to log')
            @endif
    </script>
    <h1 class="font-mono font-bold text-5xl text-center text-yellow-900 mt-9"> Contact</h1>
    <div class="flex items-center mt-5 bg-gray-50 dark:bg-gray-900 mb-20">
        <div class="container mx-auto mb-2">
            <div class="max-w-md mx-auto bg-white p-5 rounded-md shadow-sm">
                <div class="text-center">
                    <h1 class="my-3 text-3xl font-semibold text-yellow-900 dark:text-yellow-900">Contacteer mij</h1>
                    <p class="text-gray-400 dark:text-gray-400">Vul onderstaand formulier in om mij een bericht te sturen.</p>
                </div>
                <div class="m-7">
                    <form action="{{route('sendmail')}}" method="POST" id="form">
                        @csrf
                        <input type="checkbox" name="botcheck" id="" style="display: none;">
                        <div class="mb-6">
                            <label for="name" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Volledige naam</label>
                            <input type="text" name="name" id="name" placeholder="John Doe"  class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                        </div>
                        <div class="mb-6">
                            <label for="email" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Email adres</label>
                            <input type="email" name="email" id="email" placeholder="you@company.com"  class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                        </div>
                        <div class="mb-6">
                            <label for="message" class="block mb-2 text-sm text-gray-600 dark:text-gray-400">Uw bericht</label>

                            <textarea rows="5" name="message" id="message" placeholder="Your Message" class="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500"></textarea>
                        </div>
                        <div class="mb-6">
                            <button type="submit" id="submit" class="w-full px-3 py-4 text-white bg-yellow-900 rounded-md focus:bg-yellow-800 focus:outline-none">Send Message</button>
                        </div>
                        <p class="text-base text-center text-gray-400" id="result">
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
