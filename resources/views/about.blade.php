@extends('master')
@section('css')
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
@endsection

@section('main')
    <h1 class="font-mono font-bold text-5xl mt-4 text-yellow-900 text-center">Over Mij</h1>
    <div id="aboutme" class="flex lg:flex-row justify-center mt-12 mb-72 sm:flex-col-reverse">

        <div class="w-6/12 lg:mr-12 flex flex-col sm:mx-auto sm:w-10/12 mt-5">
            <p class="mb-5"> Hallo en welkom op mijn blog!</p>


            <p>Wie ben ik in een notendop?</p>
            <p class="mb-5">Ik ben Maarten Vansever, 21 jaar en student Software Engineer aan Howest Brugge. De opleiding van 3 jaar zit er bijna op, eind juni studeer ik af. Wat ik hierna zal doen, weet ik nog niet met zekerheid. Verder studeren of een job als webdesigner zoeken, de tijd zal het uitwijzen.</p>

            <p class="mb-5"> Dat programmeren een hobby is van mij, bewijst mijn studentonderneming ‘Webdream’ die ik 1 jaar geleden opgericht heb. Ik bied kwalitatieve websites aan voor een betaalbare prijs.</p>

            <p class="mb-5">  Momenteel loop ik stage bij ‘Dataline Solutions’ in Loppem. Ik vind het een hele toffe ervaring met leuke collega’s.</p>

            <p class="mb-5"> Behalve het programmeren heb ik nog andere hobby’s: padel en tennis. Bovendien organiseer ik in de schoolvakanties tennissportkampen voor kinderen en geef ik ’s avonds tennisles.</p>

            <p class="mb-5"> Leergierig, sociaal, sportief, extravert en een bezige bij zijn volgens mij vijf kenmerken die me typeren.</p>

            <p> Nu weten jullie wat meer over mij, veel plezier bij het lezen van mijn blog!</p>
        </div>
        <img src="{{asset('images/maarten.jpg')}}" title="Maarten Vansever" class="lg:mr-12 lg:h-96 rounded mx-auto" alt="Maarten Vansever">
    </div>
@endsection
