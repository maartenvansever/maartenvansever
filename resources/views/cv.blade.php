<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <title>Maarten Vansever</title>
</head>
<script>
    document.addEventListener('DOMContentLoaded', (event) => {
        document.querySelectorAll('code').forEach((block) => {
            hljs.highlightBlock(block);
        });
    });
</script>
<body class="bg-gray-50">
<header>
    <nav class="bg-yellow-900 text-center lg:h-10 sm:h-64">
        <ul class="leading-9 flex flex-row justify-center sm:flex-col lg:flex-row">
            <li class="list-none inline-block ml-8">
                <a href="{{ route('home') }}" class="text-white font-bold">Home</a>
            </li>
            <li class="inline-block ml-8">
                <a href="{{ route('cv') }}" class="text-white font-bold">CV</a>
            </li>
            <li class="inline-block ml-8">
                <a href="{{ route('aboutme') }}" class="text-white font-bold">Over Mij</a>
            </li>
            <li class="inline-block ml-8">
                <a href="{{ route('contact') }}" class="text-white font-bold">Contact</a>
            </li>
            <li class="inline-block ml-8">
                <a href="{{ route('projecten') }}" class="text-white font-bold">projecten</a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <h1 class="font-mono font-bold text-5xl mt-4 text-center text-yellow-900 mt-9"> Curriculum Vitae</h1>
    <img src="{{ asset('images/cv.jpg') }}" class="w-8/12 ml-auto mr-auto mb-10 mt-10" alt="cv" title="cv">
</main>
<footer class="bg-yellow-900 flex justify-center flex-col">
    <div class="flex justify-center flex-row">
        <a href="https://gitlab.com/maartenvansever" target="_blank">
            <i class="fab fa-gitlab text-white text-2xl w-16 hover:opacity-50"></i>
        </a>
        <a href="https://www.linkedin.com/in/maarten-vansever" target="_blank">
            <i class="fab fa-linkedin text-white text-2xl w-16 hover:opacity-50"></i>
        </a>
        <a href="mailto:maartenvansever@telenet.be" target="_blank" class="">
            <i class="far fa-envelope text-white text-2xl w-16 hover:opacity-50"></i>
        </a>
    </div>
    <div class="flex justify-center">
        <span class="text-white font-bold text-center">Made by Maarten Vansever</span>
    </div>
</footer>
</body>
</html>
